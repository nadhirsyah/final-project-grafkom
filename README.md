# Bedroom Project
Repositori _git_ ini berisi kode sumber untuk program Bedroom yang dibuat oleh **Kilimpik Grifkim**. Program ini dibuat untuk memenuhi pengerjaan **Proyek Akhir** mata kuliah Grafika Komputer Semester Genap 2019/2020, Fakultas Ilmu Komputer, Universitas Indonesia.

Anggota Kilimpik Grifkim:
1. Gagah Pangeran Rosfatiputra - 1706039566
2. Giovan Isa Musthofa - 1706040126
3. Muhammad Nadhirsyah Indra -1706039383
4. Roshani Ayu Pranasti - 1706026052

## Cara Menjalankan Program
Catatan: Chrome dan Mozilla (mungkin juga _browser_ lainnya) tidak dapat langsung menjalankan _script_ dari folder tersebut karena alasan keamanan. Solusinya adalah menjalankannya melalui **_webserver/localhost_** atau **menonaktifkan _web security_**.

Menjalankan melalui _webserver/localhost_:
1. Menggunakan python versi 3 atau lebih dengan menuliskan perintah `python -m http.server` atau `python3 -m http.server` pada terminal.
2. Membuka _browser_ dan program dapat diaskes via `http://localhost:8000/`

## Proses Pembentukan Objek
1. Membuat seluruh objek dan _scene_ pada aplikasi **Blender**.
2. Setiap objek memiliki **_Mesh_** nya masing-masing.

## Proses Rendering Objek dan Scene
1. File Blender yang sebelumnya sudah dibuat lalu di-_export_ ke dalam _file_ berbentuk **Wavefront (.obj)**.
2. Men-_convert_ _file_ Wavefront (.obj) tersebut ke dalam _file_ **.json** menggunakan _library_ `assimp2json`.
3. Memasukkan _file_ .json ke dalam _project_.
4. Me-_render_ setiap _Mesh_ yang ada pada file .json untuk membuat objek dan scene. Implementasi dapat dilihat pada file BedroomScene.js.

## Fasilitas WebGL yang Digunakan
- assimp2json: https://github.com/acgessler/assimp2json

## Algoritma-algoritma yang Digunakan
- aaa

## Log Pekerjaan
Semua anggota kelompok tidak hanya fokus kepada hal yang mereka kerjakan saja, tetapi juga fokus ke hal lain sebagai ajang untuk saling membantu jika ada yang kesusahan.

Gagah Pangeran Rosfatiputra (1706039566)

- aaa

Giovan Isa Musthofa (1706040126)

- aaa

Muhammad Nadhirsyah Indra (1706039383)

- aaa

Roshani Ayu Pranasti (1706026052)

- Membuat seluruh objek serta _scene_ pada Blender dan me-_render_-nya ke dalam WebGL.
- Mengimplementasikan opsi dua mode objek, yaitu _wireframe_ dan _shading_.
- Membuat seluruh tampilan HTML dan CSS.
- Membuat dokumentasi pada README.md.

## Referensi
https://youtu.be/fNK1E5DdYxk